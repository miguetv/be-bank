from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username) #model se hereda de BaseUserManager
        user.set_password(password)
        user.save(using=self._db) # ORM inserta usuario en base de datos
        return user
            
    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
            username=username,
            password=password,
        ) # rehuso el primer metodo
        user.is_admin = True # Le ofrezco privilegios de admin
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField('Username', max_length = 15, unique=True)
    password = models.CharField('Password', max_length = 256)
    name = models.CharField('Name', max_length = 30)
    email = models.EmailField('Email', max_length = 100)
    # se sobreescribe la función para encriptar la contraseña en la bd
    def save(self, **kwargs): # ** -> recibe un diccionario
        if not self.password:
            raise ValueError('Users must have an password')
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' # Valor secreto para cifrado
        self.password = make_password(self.password, some_salt) # Cifra la contraseña ingresada
        super().save(**kwargs)
        
    objects = UserManager()
    USERNAME_FIELD = 'username'

