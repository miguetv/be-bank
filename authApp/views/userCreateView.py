from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from authApp.serializers.userSerializer import UserSerializer

class UserCreateView(views.APIView):

    def post(self, request, *args, **kwargs):

        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        # Se obtienen los datos del token
        tokenData = {"username":request.data["username"],"password":request.data["password"]}
        # se convierte el token en un JWT valido
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        # Se valida el token
        tokenSerializer.is_valid(raise_exception=True)
        # Se retorna el codigo 201 de creación satisfactoria
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):

        return Response({'mensaje':'Hola mundo'},status=status.HTTP_200_OK)